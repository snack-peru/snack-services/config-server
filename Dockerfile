FROM openjdk:8-alpine
LABEL maintainer="Sergio Rivas Medina -> sergiorm96@gmail.com"

ADD ./target/ /app
EXPOSE 8888
WORKDIR /app
ENTRYPOINT ["java", "-jar", "config-server-0.0.1-SNAPSHOT.jar"]